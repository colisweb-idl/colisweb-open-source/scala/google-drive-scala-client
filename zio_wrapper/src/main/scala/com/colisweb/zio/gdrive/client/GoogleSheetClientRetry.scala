package com.colisweb.zio.gdrive.client

import com.colisweb.gdrive.client.GoogleUtilities.{convertColumnIndexToLetter, getRowDataHeaders, getRowValues}
import com.colisweb.gdrive.client.sheets._
import com.colisweb.gdrive.client.{EmptySpreadsheet, GoogleAuthenticator, NoColumnInSpreadsheet, SpreadsheetError}
import com.google.api.services.sheets.v4.model.{BatchUpdateSpreadsheetResponse, RowData, SheetProperties}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import eu.timepit.refined.types.numeric.NonNegInt
import zio.{IO, Schedule, Task, ZIO}

class GoogleSheetClientRetry(
    authenticator: GoogleAuthenticator,
    retryPolicy: Schedule[Any, Throwable, Any] = RetryPolicies.default
) {

  private val retry  = new Retry(retryPolicy)
  private val client = new GoogleSheetClient(authenticator)

  def createSpreadsheet(name: String, sheetsProperties: List[GoogleSheetProperties]): Task[String] =
    retry(
      client.createSpreadsheet(name, sheetsProperties)
    )

  def maybeReadSpreadsheet[T](
      id: String,
      ranges: List[String],
      parseFields: List[List[String]] => List[T]
  ): Task[List[T]] =
    retry(
      client.maybeReadSpreadsheet(id, ranges, parseFields)
    )

  def readRows(id: String, range: String): Task[Seq[RowData]] =
    retry(
      client.readRows(id, range)
    )

  def readRows(id: String, ranges: List[String]): Task[Seq[Seq[RowData]]] =
    retry(
      client.readRows(id, ranges)
    )

  def writeRanges(
      id: String,
      sheets: List[SheetRangeContent],
      inputOption: InputOption = InputOptionRaw
  ): Task[Unit] =
    retry(
      client.writeRanges(id, sheets, inputOption)
    ).unit

  def writeRange(
      id: String,
      sheet: SheetRangeContent,
      inputOption: InputOption = InputOptionRaw
  ): Task[Unit] =
    retry(
      client.writeRange(id, sheet, inputOption)
    ).unit

  def retrieveSheetsIds(id: String): Task[Map[String, Int]] =
    retry(
      client.retrieveSheetsIds(id)
    )

  def retrieveSheetsProperties(id: String): Task[List[SheetProperties]] =
    retry(
      client.retrieveSheetsProperties(id)
    )

  def batchRequests(spreadsheetId: String, requests: List[GoogleBatchRequest]): Task[Unit] =
    retry(
      client.batchRequests(spreadsheetId, requests)
    ).unit

  def batchRequestsWithResponse(
      spreadsheetId: String,
      requests: List[GoogleBatchRequest]
  ): Task[BatchUpdateSpreadsheetResponse] =
    retry(
      client.batchRequests(spreadsheetId, requests)
    )

  def getHeaders(
      spreadsheetId: GoogleSpreadsheetId
  ): IO[SpreadsheetError, Map[String, NonNegInt]] =
    readRows(spreadsheetId.value.value, "1:1")
      .mapError(error => SpreadsheetError(error.getMessage))
      .flatMap { rows =>
        rows.headOption match {
          case Some(line) => ZIO.succeed(getRowDataHeaders(line))
          case None       => ZIO.fail(EmptySpreadsheet)
        }
      }

  private[client] def getColumnIndex(
      columnName: String
  )(implicit spreadsheetId: GoogleSpreadsheetId, headers: Map[String, NonNegInt]): IO[SpreadsheetError, NonNegInt] =
    ZIO.getOrFailWith(NoColumnInSpreadsheet(spreadsheetId, columnName, 1))(headers.get(columnName))

  def buildRangeCell(
      columnName: String,
      value: String
  )(implicit
      spreadsheetId: GoogleSpreadsheetId,
      headers: Map[String, NonNegInt],
      rowIndex: NonNegInt
  ): ZIO[Any, SpreadsheetError, SheetRangeContent] = {
    getColumnIndex(columnName).map(index =>
      SheetRangeContent(s"${convertColumnIndexToLetter(index)}$rowIndex", List(List(value)))
    )
  }

  def writeRanges(
      spreadsheetId: GoogleSpreadsheetId,
      ranges: List[SheetRangeContent]
  ): Task[Unit] =
    writeRanges(spreadsheetId.value.value, ranges, InputOptionUserEntered)

  def getFirstFreeRowIndex(
      spreadsheetId: GoogleSpreadsheetId,
      headers: Map[String, NonNegInt],
      columnsToCheck: List[String]
  ): IO[SpreadsheetError, NonNegInt] = {
    getColumnsData(spreadsheetId, headers, columnsToCheck).map { case GoogleSpreadsheet(rows) =>
      val lastIndex = rows.find(_.cells.isEmpty) match {
        case Some(SpreadsheetRow(index, _)) => index.value
        case None                           => rows.lastOption.fold(1)(_.index.value) + 1
      }
      Refined.unsafeApply(lastIndex)
    }
  }

  private def getColumnsData(
      spreadsheetId: GoogleSpreadsheetId,
      headers: Map[String, NonNegInt],
      columns: List[String]
  ): IO[SpreadsheetError, GoogleSpreadsheet] = {
    val computation = for {
      columnRanges <- buildColumnRangeRequests(spreadsheetId, columns, headers)
      results      <- readRows(spreadsheetId.value.value, columnRanges)
    } yield {
      val longestResult = results.map(_.size).max
      val spreadsheetRows = results
        .map(_.padTo(longestResult, null))
        .transpose
        .zipWithIndex
        .map { case (resultPerColumn, index) =>
          SpreadsheetRow(
            Refined.unsafeApply(index + 2), // header row is skipped + index start at 1
            columns
              .zip(resultPerColumn.toList)
              .map { case (column, result) => getRowValues(result, List(column)) }
              .reduce(_ ++ _)
          )
        }
        .toList
      GoogleSpreadsheet(spreadsheetRows)
    }
    computation.mapError(error => SpreadsheetError(error.getMessage))
  }

  private def buildColumnRangeRequests(
      spreadsheetId: GoogleSpreadsheetId,
      columns: List[String],
      headers: Map[String, NonNegInt]
  ): ZIO[Any, SpreadsheetError, List[String]] = {
    ZIO.foreach(columns) { column =>
      headers.get(column) match {
        case Some(index) =>
          val columnLetter             = convertColumnIndexToLetter(index.value)
          val columnRangeWithoutHeader = s"${columnLetter}2:$columnLetter"
          ZIO.succeed(columnRangeWithoutHeader)
        case None => ZIO.fail(NoColumnInSpreadsheet(spreadsheetId, column, Refined.unsafeApply(1)))
      }
    }
  }

  def extendSheetIfNeeded(
      spreadsheetId: GoogleSpreadsheetId,
      firstFreeRow: Int,
      minimum: Int = 100,
      sizeIncrease: Int = 200,
      sheetIndex: Option[Int] = None
  ): IO[SpreadsheetError, Unit] = {

    def extendWhenNeeded(leftover: Int, sheet: SheetProperties): Task[Unit] =
      ZIO
        .when(leftover < minimum) {
          val request = InsertDimension(sheet.getSheetId, Rows, firstFreeRow, firstFreeRow + sizeIncrease)
          batchRequests(spreadsheetId.value.value, List(request))
        }
        .unit
    val execution = for {
      sheetsProperties <- retrieveSheetsProperties(spreadsheetId.value.value)
      sheet    = sheetIndex.fold(sheetsProperties.minBy(_.getSheetId))(sheetsProperties)
      leftover = sheet.getGridProperties.getRowCount - firstFreeRow
      _ <- extendWhenNeeded(leftover, sheet)
    } yield ()

    execution.mapError(error => SpreadsheetError(error.getMessage))
  }

}
