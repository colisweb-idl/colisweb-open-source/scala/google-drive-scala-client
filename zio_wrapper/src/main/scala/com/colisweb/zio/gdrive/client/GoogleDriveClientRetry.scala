package com.colisweb.zio.gdrive.client

import com.colisweb.gdrive.client.GoogleAuthenticator
import com.colisweb.gdrive.client.GoogleUtilities._
import com.colisweb.gdrive.client.drive.GoogleDriveRole.GoogleDriveRole
import com.colisweb.gdrive.client.drive.{GoogleDriveClient, GoogleMimeType, GoogleSearchResult}
import com.google.api.services.drive.model.{FileList, Permission}
import zio.{Schedule, Task, ZIO}

import java.io.{File, InputStream}

class GoogleDriveClientRetry(
    authenticator: GoogleAuthenticator,
    retryPolicy: Schedule[Any, Throwable, Any] = RetryPolicies.default
) {

  private val retry  = new Retry(retryPolicy)
  private val client = new GoogleDriveClient(authenticator)

  def uploadFile(
      folderId: String,
      file: File,
      driveFilename: String,
      filetype: GoogleMimeType,
      outputFiletype: Option[GoogleMimeType]
  ): Task[String] =
    retry(
      client.uploadFile(folderId, file, driveFilename, filetype, outputFiletype)
    )

  def createFolder(name: String, parentId: Option[String] = None): Task[String] =
    retry(
      client.createFolder(name, parentId)
    )

  def delete(fileId: String): Task[Unit] =
    retry(
      client.delete(fileId)
    ).unit

  def listFilesInFolder(folderId: String): Task[List[GoogleSearchResult]] =
    retry(
      client.listFilesInFolder(folderId)
    )

  def move(targetId: String, parentId: String): Task[Unit] =
    retry(
      client.move(targetId, parentId)
    ).unit

  def share(
      fileId: String,
      email: String,
      role: GoogleDriveRole,
      sendEmailNotification: Boolean = true
  ): Task[Permission] =
    retry(
      client.share(fileId, email, role, sendEmailNotification)
    )

  def getParents(id: String): Task[List[String]] =
    retry(
      client.getParents(id)
    )

  def listFiles(query: String): Task[FileList] =
    retry(
      client.listFiles(query)
    )

  def isInSubFolderOf(id: String, rootId: String): Task[Boolean] = {

    def step(currentId: String): Task[Boolean] =
      getParents(currentId).flatMap {
        case Nil                                 => ZIO.succeed(false)
        case parents if parents.contains(rootId) => ZIO.succeed(true)
        case next :: _                           => step(next)
      }

    step(id)
  }

  def findFileInSubFolderOf(
      keywords: String,
      rootId: String,
      maybeMimeType: Option[GoogleMimeType] = None
  ): Task[Option[GoogleSearchResult]] = {

    val mimeTypeQueryPart = maybeMimeType.fold("")(mimeType => s" and mimeType = '${GoogleMimeType.name(mimeType)}'")
    val query             = s"name contains '$keywords'" + mimeTypeQueryPart

    listFiles(query)
      .flatMap { list =>
        val files = list.getFiles.asScalaListNotNull

        ZIO.foreach(files)(file => isInSubFolderOf(file.getId, rootId)).map { result =>
          (result zip files)
            .find { case (isInSubFolder, _) => isInSubFolder }
            .map { case (_, file) => GoogleSearchResult(file.getId, file.getName) }
        }
      }
  }

  def downloadAsInputStream(fileId: String): Task[InputStream] =
    retry(
      client.downloadAsInputStream(fileId)
    )

  def downloadExcelAsInputStream(fileId: String): Task[InputStream] =
    retry(
      client.downloadExcelAsInputStream(fileId)
    )

  def downloadExcelAsBase64String(fileId: String): Task[String] =
    retry(
      client.downloadExcelAsBase64String(fileId)
    )
}
